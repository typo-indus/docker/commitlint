FROM node:22-alpine

# --------------------------------------------- Mets à jour les dépendances --------------------------------------------

RUN apk update && \
    apk upgrade && \
    rm -rf /var/cache/apk/*

# ------------------------------------------------- Installe commitlint ------------------------------------------------

RUN npm update -g && npm install -g @commitlint/cli @commitlint/config-conventional

COPY config/commitlint.config.js /config/commitlint.config.js
